"""Dictbot Tracker.
"""
import socket
import random
from datetime import date

from . import tasks, remote, store, site

logging = site.logger

class Error(Exception):
    '''Tracker error'''

class Info:
    def __init__(self, host):
        self.name = host.split(':')[0]

    def addr(self):
        try:
            return socket.gethostbyname(self.name)
        except Exception as e:
            return e

class Update(tasks.Request):
    def view(self, arg):
        servers = store.Server.all()
        for server in servers:
            task = tasks.Task('/host')
            task.schedule(server.host)

    def process(self, arg):
        log = store.Log()
        log.put()
        self.view(arg)

        conf = store.Conf.get('site')
        mins = arg or conf.mins

        task = tasks.Task('/state')
        task.time = int(mins) * 60
        task.schedule(str(conf.days))

class Host(tasks.Request):
    def view(self, arg):
        host = arg
        task = tasks.Task('/host')
        task.schedule(host)

    def process(self, arg):
        host = arg
        logging.info('Host: %s' % host)
        info = Info(host)
        logging.info('Addr: %s' % info.addr())

        conf = store.Conf.get('site')
        server = store.Server.get(host)
        extended = True if not server \
        else date.today().isoweekday() == conf.weeks

        tracker = remote.Tracker(host)
        result = tracker.run(extended)

        fmt = lambda l: '|'.join(l)
        vals = [fmt(val) for val in result]
        buf = '\n'.join(vals)

        task = tasks.Task('/store')
        task.schedule(buf)

class Schedule(tasks.Request):
    def view(self, arg):
        conf = store.Conf.get('site')
        hours = arg or conf.hours

        task = tasks.Task('/update')
        task.time = random.randrange(int(hours) * 60) * 60
        task.schedule(str(conf.mins))

        servers = store.New.server()
        for server in servers:
            task = tasks.Task('/host')
            task.schedule(server.host)
            logging.info('New: %s' % server.host)


routes = [
    ('/schedule', Schedule),
    ('/update', Update),
    ('/host', Host)
]
