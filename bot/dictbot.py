"""Dictbot Store.
"""
from . import store, tasks, site

logging = site.logger

class Store(tasks.Request):
    def process(self, data):
        lines = data.splitlines()
        server = lines[0]
        data = lines[1:]

        host = self.server(server)
        if data:
            self.record(host, data)

    def server(self, line):
        fields = line.split('|')
        host = fields[0]
        state = fields[1]

        server = store.Server.new(host)

        if state == 'up':
            server.ident = fields[2]
            server.capbs = fields[3]
            dbases = fields[4]
            strats = fields[5]
            if dbases:
                server.dbases = int(dbases)
            if strats:
                server.strats = int(strats)
        elif server.ident == None:  # new
            server.ident = server.capbs = ''
            server.dbases = server.strats = 0

        server.put()

        hist = store.Hist(state=state, host=host)
        hist.put()

        logging.info('%s: %s' % (host, state))

        return host

    def record(self, host, data):
        info = store.Info.new(host)

        line = data[0]
        dbases = line.split('|')
        if dbases:
            info.dblist = dbases

        line = data[1]
        strats = line.split('|')
        if strats:
            info.stratlist = strats

        logging.info('lists: %s %s' % (len(dbases), len(strats)))

        info.put()


class State(tasks.Request):
    def view(self, arg):
        conf = store.Conf.get('site')
        days = arg or conf.days

        servers = store.Server.all()
        for server in servers:
            host = server.host
            state = store.State.new(host)
            hist = server.current()
            if not hist:
                continue
            state.current = hist.state
            state.update(int(days))
            state.put()
            logging.info('%s: %s %s' % (host, state.ups, state.downs))

    def process(self, arg):
        self.view(arg)


routes = [
    ('/store', Store),
    ('/state', State)
]
