import requests
import hashlib
import json

from . import tasks, store, site

logging = site.logger

class Error(Exception):
    """Check error"""

class Conf(store.Model):
    props = 'url', 'arch'

class Check(tasks.Request):
    def view(self, arg):
        site = Conf.get('check')
        for arch in site.arch:
            url = site.url + arch
            try:
                doc = self.fetch(url)
            except Error as e:
                logging.error(e)
                return

        urls = json.loads(doc)
        for entry in urls:
            try:
                self.check(entry)
            except Error as e:
                logging.error(e)

    def fetch(self, url):
        logging.debug(url)
        try:
            response = requests.get(url)
        except Exception as e:
            raise Error(e)

        status = response.status_code
        if status != 200:
            raise Error('Status: %s' % status)

        return response.content

    def check(self, entry):
        url = entry.get('url')
        arch = self.fetch(url)

        size = len(arch)
        if size != entry.get('size'):
            raise Error('Size: %s' % size)

        check = md5(arch)
        if check != entry.get('md5'):
            raise Error('Content: %s' % check)

        logging.info('Check passed')

def md5(val):
    return hashlib.md5(val).hexdigest()

routes = [
    ('/check', Check)
]
