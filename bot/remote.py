"""Dict Connection.
"""
import pycurl
import io
import time

from . import site
logging = site.logger

class RemoteError(Exception):
    '''Remote error'''

URL = 'dict://%s/'

class Info:
    def __init__(self):
        self.ident = ''
        self.capbs = ''
        self.dbases = ''
        self.strats = ''

class Connection:
    def __init__(self, host):
        self.url = URL % host
        self.curl = pycurl.Curl()
        self.curl.setopt(pycurl.CONNECTTIMEOUT, 10)
        self.curl.setopt(pycurl.TIMEOUT, 60)
        self.info = Info()

    def close(self):
        self.curl.close()

    def command(self, cmd):
        self.curl.setopt(pycurl.URL, self.url + cmd)
        buf = io.BytesIO()
        self.curl.setopt(pycurl.WRITEFUNCTION, buf.write)
        self.curl.perform()
        return buf.getvalue().decode('utf-8')

    def send(self, request):
        if request == Request.Status:
            cmd = 'status'
        elif request == Request.Databases:
            cmd = 'show databases'
        elif request == Request.Strategies:
            cmd = 'show strategies'
        else:
            return

        self.result = self.command(cmd)
        if not self.result:
            raise RemoteError('Empty response')

        self.state = State.Reading
        self.read(self.result)

    def identification(self, text):
        pos = text.find('<')
        if pos > 0:
            self.info.ident = text[:pos].strip()
            end = text.find('>')
            if end > pos:
                self.info.capbs = text[pos + 1:end].strip()
        else:
            self.info.ident = text.strip()

    def read(self, text):
        if self.state == State.Reading:
            self.state = State.Parsing

        lines = text.split('\n')
        for line in lines:
            line = line.strip('\r')
            if self.state == State.Parsing:
                if len(line) > 0:
                    self.parse(line)
            elif self.state == State.Appending:
                if Parser.end(line):
                    self.state = State.Parsing
                else:
                    self.response.append(line)

        if self.state == State.Parsing:
            self.state = State.Reading

    def parse(self, line):
        code = Parser.code(line)
        if not code:
            logging.warn('Code not found: %s' % line)
            return
        message = Parser.message(line)

        if code == Response.Hello:
            self.identification(message);
        elif code == Response.DatabaseList:
            self.info.dbases = Parser.count(message)
            self.response = ListResponse(code)
            self.state = State.Appending
        elif code == Response.StrategyList:
            self.info.strats = Parser.count(message)
            self.response = ListResponse(code)
            self.state = State.Appending
        elif code == Response.StatusMessage:
            self.state = State.Parsing
        elif code == Response.OK:
            self.state = State.Parsing
        elif code == Response.Bye:
            pass
        else:
            self.state = State.Error


class Parser:
    @staticmethod
    def count(line):
        try:
            pos = line.index(' ')
            return int(line[:pos])
        except Exception as e:
            logging.error('Count: %s' % e)
            return 0

    @staticmethod
    def code(line):
        try:
            return int(line[:3])
        except Exception as e:
            logging.error('Code: %s' % e)
            return 0

    @staticmethod
    def message(line):
        return line[4:] if len(line) > 4 else ''

    @staticmethod
    def end(line):
        return len(line) <= 3 and line.startswith('.')


class Request:
    Quit = 0
    Client = 1
    Status = 2
    Databases = 3
    Strategies = 4

class Response:
    DatabaseList = 110
    StrategyList = 111

    StatusMessage = 210
    Hello = 220
    Bye = 221
    OK = 250

    def __init__(self, code):
        self.code = code


class ListResponse(Response):
    def __init__(self, code):
        Response.__init__(self, code)
        self.list = []

    def append(self, line):
        pair = line.split('"')
        if len(pair) > 1:
            text = pair[1].strip('"')
            if not text:
                text = pair[0]
            self.list.append(text.strip())

class State:
    Writing = 4
    Reading = 5
    Parsing = 6
    Appending = 7
    Error = 9


class Tracker:
    def __init__(self, host):
        self.host = host

    def state(self, conn):
        error_code = 0
        while True:
            try:
                conn.send(Request.Status)
                state = 'up'
                break
            except pycurl.error as e:
                code, error = e.args
                logging.error('Connection: %s' % error)
                state = 'down'
            except RemoteError as e:
                logging.error('Remote: %s' % e)
                code = -1
                state = 'up'
            except BaseException as e:
                logging.error('System: %s' % e)
                state = 'unknown'
                break

            if code != error_code:
                error_code = code
                logging.info('Retrying: %s' % code)
                time.sleep(1)
            else:  # error on retry
                break

        return state

    def run(self, extended=True):
        host = self.host
        logging.info('Connect: %s' % host)
        conn = Connection(host)
        state = self.state(conn)
        logging.info('State: %s' % state)

        info = conn.info
        if state == 'up':
            if info.ident:
                logging.debug('Ident: %s' % info.ident)
            else:
                logging.warn('Ident not found')
                logging.debug(conn.result)
                state = 'unknown'

        dbases = strats = None
        if state == 'up' and extended:
            conn.send(Request.Databases)
            if info.dbases:
                logging.debug('Dbases: %s' % info.dbases)
                dbases = conn.response.list
            else:
                logging.warn('Dbases not found')

            conn.send(Request.Strategies)
            if info.strats:
                logging.debug('Strats: %s' % info.strats)
                strats = conn.response.list
            else:
                logging.warn('Strats not found')
        conn.close()

        server = [host, state, info.ident, info.capbs, str(info.dbases), str(info.strats)]
        result = [server]
        if dbases and strats:
            result += [dbases, strats]
        return result
