import os

project = os.environ.get('GOOGLE_CLOUD_PROJECT')
service = os.environ.get('GAE_SERVICE')
location = os.environ.get('GAE_LOCATION')

import logging
FMT = '%(levelname).1s %(name)s %(message)s'

def setup():
    logging.basicConfig(format=FMT, level=0)

def client():
    from google.cloud import logging
    return logging.Client()

def cloud():
    cl = client()
    handler = cl.get_default_handler()

    logger = logging.getLogger('cloud')
    logger.propagate = False
    logger.setLevel(0)
    logger.addHandler(handler)
    return logger

def python():
    return logging.getLogger()

if service == 'backend':
    logger = cloud()
else:
    logger = python()
