"""Cloud Tasks
"""
import webapp3
from datetime import datetime, timedelta

from . import site
logging = site.logger

project = site.project
location = site.location
queue = 'default'

def client():
    from google.cloud import tasks
    return tasks.CloudTasksClient()

try:
    ct = client()
except ImportError:
    ct = None

def timestamp(time):
    import google.protobuf.timestamp_pb2 as pb2
    timestamp = pb2.Timestamp()
    schedule = datetime.utcnow() + timedelta(seconds=time)
    if time: logging.info('Schedule: %s' % schedule)
    timestamp.FromDatetime(schedule)
    return timestamp

class Task:
    def __init__(self, path):
        self.path = path
        self.target = 'backend'
        self.method = 'POST'
        self.time = 0

    def schedule(self, arg):
        parent = ct.queue_path(project, location, queue)
        task = {
            'app_engine_http_request': {
                'http_method': self.method,
                'relative_uri': self.path,
                'app_engine_routing': {
                    'service': self.target
                    },
                'body': bytes(arg, 'utf-8')
                },
            'schedule_time': timestamp(self.time)
            }
        return ct.create_task(parent, task)


class Request(webapp3.RequestHandler):
    '''Task request'''
    def param(self, arg):
        val = self.request.get(arg)
        return val.strip() if val else ''

    def get(self):
        '''Set the task'''
        try:
            arg = self.param('arg')
            self.view(arg)
        except Exception as e:
            logging.error(e)
            self.abort(424)

    def post(self):
        '''Run the task'''
        try:
            content = str(self.request.body, 'utf-8')
            self.process(content)
        except Exception as e:
            logging.error(e)
            self.abort(424)


class System(webapp3.RequestHandler):
    '''System request'''
    def get(self, path):
        logging.debug('System request: %s' % path)

routes = [
    ('/_ah/(.+)', System)
]
