"""Firestore Models.
"""
from datetime import datetime, timedelta

def client():
    from google.cloud import firestore
    return firestore.Client()

try:
    ds = client()
except ImportError:
    ds = None

class Model:
    _added = None

    def __init__(self, **args):
        for key in self.props:
            val = args.get(key)
            setattr(self, key, val)
        self.id = None

    def dump(self):
        obj = dict()
        for key in self.props:
            val = getattr(self, key)
            if val != None:
                obj[key] = val
        return obj

    @classmethod
    def load(cls, snap):
        obj = cls(**snap.to_dict())
        obj.id = snap.id
        return obj

    @classmethod
    def name(cls):
        return cls.__name__

    @classmethod
    def col(cls):
        return ds.collection(cls.name())

    @classmethod
    def new(cls, oid):
        obj = cls.get(oid)
        if not obj:
            obj = cls()
            obj.id = oid
        return obj

    @classmethod
    def get(cls, oid):
        doc = cls.col().document(oid)
        snap = doc.get()
        if snap and snap.exists:
            return cls.load(snap)

    def set(self, oid):
        self._setup()
        doc = self.col().document(oid)
        doc.set(self.dump())

    def _setup(self):
        if self._added and not getattr(self, self._added):
            setattr(self, self._added, datetime.utcnow())

    def put(self):
        if self.id:  # update doc
            self.set(self.id)
        else:  # add doc
            self._setup()
            self.col().add(self.dump())

    def delete(self):
        doc = self.col().document(self.id)
        return doc.delete()

    @classmethod
    def all(cls):
        ids = cls.col().list_documents()
        return list(cls.mget(ids))

    @classmethod
    def mget(cls, ids):
        for snap in ds.get_all(ids):
            if snap and snap.exists:
                yield cls.load(snap)

    @classmethod
    def fetch(cls, query, limit=10):
        docs = query.limit(limit).get()
        for snap in docs:
            yield cls.load(snap)

    @classmethod
    def first(cls, query):
        docs = list(cls.fetch(query, 1))
        if docs: return docs[0]


class RefModel(Model):
    def __getattr__(self, arg):
        try:
            cls = eval(arg.title())
            return cls.get(self.id)
        except NameError:
            pass
        raise AttributeError(arg)

    @classmethod
    def map(cls, docs=None):
        return {doc.id: doc for doc in docs or cls.all()}

    @property
    def host(self):
        return self.id


class Log(Model):
    _added = 'date'
    props = 'date',

    @classmethod
    def current(cls):
        query = cls.col().order_by('date', direction='DESCENDING')
        return cls.first(query)


class Hist(Model):
    _added = 'date'
    props = 'host', 'state', 'date'

    @classmethod
    def server(cls, host):
        return cls.col().where('host', '==', host) \
                .order_by('date', direction='DESCENDING')

    @classmethod
    def count(cls, host, state, days):
        date = datetime.utcnow() - timedelta(days=days)
        query = cls.server(host).where('date', '>', date) \
                .where('state', '==', state)
        return len(list(query.get()))


class Server(RefModel):
    props = 'ident', 'capbs', 'dbases', 'strats'

    def current(self):
        query = Hist.server(self.host)
        return Hist.first(query)


class State(RefModel):
    props = 'current', 'ups', 'downs'

    def update(self, days=365):
        count = Count(self.server.host, days)
        self.ups = count.state('up')
        self.downs = count.state('down') + count.state('unknown')

    @property
    def avail(self):
        return 100 * self.ups // (self.ups + self.downs)


class Count:
    def __init__(self, host, days):
        self.host = host
        self.days = days

    def state(self, state):
        return Hist.count(self.host, state, self.days)


class Info(RefModel):
    props = 'dblist', 'stratlist'


class New(Model):
    _added = 'date'
    props = 'host', 'date'

    @classmethod
    def server(cls, days=1):
        date = datetime.utcnow() - timedelta(days=days)
        query = cls.col().where('date', '>', date)
        return cls.fetch(query)


class Conf(Model):
    props = 'days', 'hours', 'mins', 'weeks'


class Auth(Model):
    props = 'val',


class Block(Model):
    props = 'bots', 'nets'
