from bot import site
site.setup()

if site.service == 'backend':
    from bot import tasks, tracker, dictbot, check
    routes = tasks.routes + tracker.routes + dictbot.routes + check.routes
else:
    from view import web, main, servers, api
    routes = web.routes + servers.routes + api.routes + main.routes

import webapp3
app = webapp3.WSGIApplication(routes)
