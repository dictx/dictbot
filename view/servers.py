"""Servers Handler.
"""
from operator import attrgetter

from bot import store
from . import web, util

class FormError(Exception):
    '''Form error'''

class Web(web.Request):
    def render(self, values):
        template = util.template(self.template)
        self.response.write(template.render(values))

    def process(self):
        try:
            values = self.result()
        except FormError as e:
            self.template = 'error.html'
            values = {'error': e}
        except AttributeError:
            raise web.MethodError
        self.render(values)


class List(Web):
    template = 'list.html'

    def view(self):
        states = []
        servers = store.Server.map()

        for state in store.State.all():
            if state.current == 'up' or state.ups > state.downs:
                state.server = servers[state.host]
                states.append(state)

        states.sort(key=attrgetter('avail', 'ups'), reverse=True)
        update = store.Log.current()

        values = {'servers': states, 'update': update}
        self.render(values)


class Server(Web):
    template = 'server.html'

    def view(self):
        host = self.request.get('host')
        if not host:
            raise web.ResourceError('Host not found')

        server = store.Server.get(host)
        if not server:
            raise web.ResourceError('Server not found: %s' % host)

        info = server.info
        if not info:
            raise web.ResourceError('Info not found: %s' % host)

        values = {'server': server, 'info': info}
        self.render(values)


class New(Web):
    template = 'new.html'

    def view(self):
        self.template = 'add.html'
        self.render({})

    def result(self):
        host = self.request.get('host')
        host = host.strip().lower()

        import re
        match = re.match(r'([a-z0-9_-]+\.)+[a-z]+:?[0-9]*', host)

        if not match:
            raise FormError('Invalid host name')

        newhost = store.New(host=host)
        newhost.put()
        return {}


class Search(Web):
    template = 'search.html'

    def view(self):
        self.process()

    def result(self):
        search = self.request.get('query')
        search = search.strip()

        if len(search) < 2:
            raise FormError('At least 2 characters required for search')

        dblist = self.find(search)
        return {'dblist': dblist}

    def find(self, search):
        dblist = {}

        for info in store.Info.all():
            if not info:
                continue
            for dbase in info.dblist:
                if search.lower() in dbase.lower():
                    if dbase not in dblist:
                        dblist[dbase] = []
                    if info.host not in dblist[dbase]:
                        dblist[dbase].append(info.host)

        result = {}
        if dblist:
            servers = store.State.map()
        for dbase in dblist:
            pos = dbase.lower().find(search.lower())
            off = len(search)
            parts = dbase[:pos], dbase[pos:pos + off], dbase[pos + off:]
            key = '%s|%s|%s' % parts
            result[key] = [servers[host] for host in dblist[dbase]]
        return result


routes = [
    ('/servers', List),
    ('/servers/', Server),
    ('/servers/new', New),
    ('/servers/search', Search)
]
