"""Site Config.
"""
import logging

from bot import store

class Error(Exception):
    """Conf error"""

class Headers(object):
    def response(self):
        return {
            'X-Frame-Options': 'SAMEORIGIN',
            'X-XSS-Protection': '1; mode=block',
            'X-Robots-Tag': 'noarchive',
        }

    def gae(self, headers):
        XAE = 'X-Appengine-'
        buf = []
        for key, val in headers.items():
            if key.startswith(XAE) and 'Default' not in key:
                buf.append('%s: %s' % (key[len(XAE):], val))
        if buf:
            logging.info('; '.join(buf))

    def request(self, request):
        self.gae(request.headers)

        ua = request.headers.get('User-Agent')
        if not ua:
            raise Error('No user agent')
        logging.info(ua)
        if request.path == '/':
            return

        # bots that don't respect instructions
        if block.bot(ua):
            raise Error('Rejected bot')

        addr = request.environ.get('REMOTE_ADDR')
        if not addr:
            logging.error('No address found')
            return

        # bots with fake user agent
        if block.net(addr):
            raise Error('Rejected fake')

class Block(object):
    def __init__(self, name):
        try:
            block = store.Block.get(name)
            if not block:
                raise Error('Block not found')
        except BaseException as e:
            logging.critical(e)
            block = store.Block()

        self.bots = []
        for bot in block.bots:
            self.bots.append(bot.lower())

        self.nets = []
        for net in block.nets:
            addr, mask = net.split('/')
            sub = 4 - int(mask) // 8
            vals = addr.rsplit('.', sub)
            self.nets.append(vals[0] + '.')

    def bot(self, ua):
        for bot in self.bots:
            if bot in ua.lower():
                return True

    def net(self, addr):
        for net in self.nets:
            if addr.startswith(net):
                return True

block = Block('site')
