"""Request Handler.
"""
import webapp3
import webapp3_extras.routes

import logging

from . import conf

class Error(Exception):
    code = 400

class AuthError(Error):
    code = 401

class AccessError(Error):
    code = 403

class ResourceError(Error):
    code = 404

class MethodError(Error):
    code = 405


class Request(webapp3.RequestHandler):
    def get(self, *args):
        try:
            self.init()
            self.view(*args)
        except Error as e:
            logging.error(e)
            self.abort(e.code)

    def post(self, *args):
        try:
            self.init()
            self.process()
        except Error as e:
            logging.error(e)
            self.abort(e.code)

    def init(self):
        headers = conf.Headers()
        self.response.headers.update(headers.response())
        try:
            headers.request(self.request)
        except conf.Error as e:
            raise AccessError(e)

    def view(self):
        raise MethodError('Not implemented')

    def process(self):
        raise MethodError('Not implemented')


class Auth(Request):
    def auth(self):
        from bot import store
        from . import auth
        try:
            user, pswd = auth.authentication(self.request)
        except auth.Error as e:
            raise AuthError(e)

        site = store.Auth.get(user)
        if not site:
            raise AuthError('Unknown user: %s' % user)

        import hashlib
        secret = hashlib.md5(pswd.encode('utf-8')).hexdigest()
        if site.val != secret:
            raise AuthError('Wrong secret: %s' % pswd)

    def init(self):
        super(Auth, self).init()
        self.auth()


class DomainRoute(webapp3_extras.routes.DomainRoute):
    def match(self, request):
        host_match = self.regex.match(request.host.split(':', 1)[0])
        if host_match:
            args, kwargs = webapp3._get_route_variables(host_match)
            return webapp3_extras.routes._match_routes(self.get_match_children, request, None, kwargs)

class Redir(webapp3.RequestHandler):
    def get(self, args):
        path = 'https://%s%s' % ('dikt.tv', self.request.path_qs)
        self.redirect(path, permanent=True)

def domain(redir):
    return DomainRoute(redir, [webapp3.Route('/<:.*>', Redir)])

def scheme(redir):
    return webapp3_extras.routes.RedirectRoute('/<:.*>', Redir, schemes=[redir])

routes = [domain('dictbot.appspot.com'), domain('www.dikt.tv'), scheme('http')]
