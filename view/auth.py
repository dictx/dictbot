"""Request Authentication.
"""
import base64

from urllib import parse
import logging


class Error(Exception):
    """Raised on auth errors."""

def authentication(request):
    auth = basic(request)
    if not auth:
        raise Error('Authentication not found')

    try:
        encoding = request.encoding
    except AttributeError:
        encoding = 'utf-8'

    try:
        b64_decoded = base64.b64decode(auth)
    except TypeError:
        logging.error('Base64 decode failed for: %s', auth)
        raise Error('Invalid Authentication')

    try:
        string_decoded = b64_decoded.decode(encoding)
    except UnicodeDecodeError:
        logging.error('String decode failed for: %s', string_decoded)
        raise Error('Invalid Authentication')

    return map(parse.unquote_plus, string_decoded.split(':', 1))


def authorization(request, auth_type):
    auth = request.headers.get('Authorization', None)
    if not auth:
        raise Error('Authorization not found')

    _type, _auth = auth.split(' ', 1)

    if _type != auth_type:
        raise Error('Invalid authorization type')

    return _auth

def basic(request):
    return authorization(request, 'Basic')
