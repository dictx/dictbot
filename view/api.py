"""Servers API.
"""
from . import servers

class Api(servers.List):
    def render(self, values):
        servers = {}
        for state in values.get('servers'):
            server = state.server
            servers[state.host] = {'state': state.current,
                                   'availability': '%s%%' % state.avail,
                                   'capabilities': server.capbs.split('.'),
                                   'databases': server.dbases,
                                   'strategies': server.strats}

        update = values.get('update').date.isoformat().split('.')[0] + 'Z'
        template = {'updated': update, 'servers': servers}
        
        self.response.write(self.output(template))
        self.response.headers['Content-Type'] = self.content_type

class Json(Api):
    content_type = 'application/json'
    
    def output(self, template):
        import json
        return json.dumps(template, indent=2)

class Yaml(Api):
    content_type = 'text/yaml'

    def output(self, template):
        import yaml
        return yaml.dump(template, sort_keys=False)

routes = [
    ('/servers\.json', Json),
    ('/servers\.yaml', Yaml)
]
