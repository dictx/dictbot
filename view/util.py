"""Utils.
"""
import jinja2
j2env = jinja2.Environment(loader=jinja2.FileSystemLoader('view/tmpl'))

def template(name):
    return j2env.get_template(name)

def from_file(name):
    with open(name, 'rb') as f:
        return f.read()
