"""Main Handler.
"""
from bot import store
from . import web, util

class Page(web.Request):
    def view(self, path='main'):
        try:
            page = util.from_file('view/site/%s.html' % path)
        except IOError:
            raise web.ResourceError('Path not found: %s' % path)
        self.response.write(page)


class Archive(web.Request):
    def view(self, archive=None):
        site = store.Conf.get('dist')
        if not archive:
            archive = site.arch[-1]
        elif archive not in site.arch:
            raise web.ResourceError('Archive not found: %s' % archive)
        url = site.url + archive
        self.redirect(str(url))


class Rename(web.Auth):
    def process(self):
        host = self.request.get('host')
        new = self.request.get('new')

        if not host or not new:
            return

        server = store.Server.get(host)
        if not server:
            return

        info = store.Info.get(host)
        if info:
            info.delete()

        state = store.State.get(host)
        if state:
            state.delete()

        newserver = store.Server(host=new)
        newserver.set(new)

        docs = store.Hist.server(host)
        for snap in docs.get():
            hist = store.Hist.load(snap)
            newhist = store.Hist(host=new, date=hist.date,
                                 state=hist.state)
            hist.delete()
            newhist.put()

        server.delete()

        self.response.write('Renamed %s to %s\n' % (host, new))


class Remove(web.Auth):
    def process(self):
        host = self.request.get('host')
        if not host:
            return

        server = store.Server.get(host)
        if server:
            info = store.Info.get(host)
            if info:
                info.delete()

            state = store.State.get(host)
            if state:
                state.delete()

            docs = store.Hist.server(host)
            for snap in docs.get():
                hist = store.Hist.load(snap)
                hist.delete()

        else:
            server = store.New.get(host)
            if not server:
                return

        server.delete()

        self.response.write('Removed %s\n' % host)


routes = [
    ('/', Page),
    ('/archive', Archive),
    ('/rename', Rename),
    ('/remove', Remove),
    ('/(.+)', Page)
]
