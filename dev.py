from bot import site
site.setup()

from view import main, servers, api

if __name__ == '__main__':
    from paste import httpserver, cascade, urlparser
    static = urlparser.StaticURLParser('web')
    routes = servers.routes + api.routes + main.routes
    import webapp3
    app = webapp3.WSGIApplication(routes)
    webapp = cascade.Cascade([static, app])
    httpserver.serve(webapp, host='localhost', port='8080')
