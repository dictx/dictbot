.. _KDE: http://www.kde.org/

Installation
============

Dikt is a `KDE`_ application.
If you already use KDE, first run :command:`cmake`,
to configure Dikt for your system,
then :command:`make`, to build and install Dikt.

Usually you would build the application in a separate directory,
and pass some options to cmake,
typically to build an optimized release build.

In Dikt directory type::

    mkdir build
    cd build

    cmake -DCMAKE_BUILD_TYPE=Release ..
    make

    # as superuser
    make install

And few minutes later, your dictionary will be ready for use.

If you don't have KDE, but you still want to use Dikt, then you will
have to install Qt and KDE libraries. It is enough to install the
libraries, it is not necessary to run KDE to use Dikt. Dikt will work
on any desktop.
