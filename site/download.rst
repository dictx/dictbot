Download
========

Source
------

Source releases are hosted on `OSDN Service <https://osdn.net/>`_.

Current version is 2s7:

https://osdn.net/dl/dikt/dikt-2s7.txz


Packages
--------

Installation packages can be found in the most popular Open Source distributions.

`Packages Search <https://pkgs.org/>`_ can help you to find packages for your system:

https://pkgs.org/download/dikt
