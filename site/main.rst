.. title:: Home

.. toctree::
    :hidden:

    download
    features
    installation
    formatting


Dikt is a dictionary application that implements the Dict protocol.

Dikt is a network application, which looks up words on Dict servers,
and to use it, it is not necessary to install any dictionary files.


Features
--------

Dikt user interface is very similar to web browsers,
it should be easy to use for anyone familiar with the Web and Internet.

See the `screenshot <_static/dikt.png>`_

Dikt also has some advanced features for experienced users.

See the list of :doc:`features`.


Installation
------------

It should take only few minutes to install Dikt and start using it.

Installation packages can be found in the most popular Open Source distributions.

See :doc:`download` page for packages.

Or if you prefer to build it yourself from the source.

See :doc:`installation` instructions.


Contact
-------

For questions, comments, suggestions,
the email address is info@dikt.tv.
