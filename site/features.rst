.. _Dict Project: http://www.dict.org/

Features
========

Dict protocol
    Dikt implements the client side of Dict protocol,
    which lets you make advanced dictionary searches.
    See `Dict Project`_ web site for information on Dict protocol.

Network application
    Dikt is a network application, it looks up words on Dict servers,
    so it is not necessary to install any dictionaries.

Browser interface
    Dikt is very similar to web browsers and should be easy to use
    for anyone familiar with the Web and Internet.

Search history
    Dikt lets you navigate through past definitions and matches.

HTML formatting
    Dikt converts plain text entries into HTML text which is much easier to read.
    It supports custom :doc:`formatting`, and also custom colors in HTML text.

Retrieving suggestions
    When no matches are found, Dikt will suggest possible corrections.

Looking up selected text
    Simply select the text anywhere on your desktop, and Dikt will look it up.

Automatic matching
    Dikt can retrieve matches with every search.

Database groups
    Several databases can be grouped together, to work like a single dictionary.

Dict Urls
    With a Dict Url in the form **dict://host/word**, look up words on any server.

...

Plus all features expected from a desktop applications:
text find, file saving, document printing, online help, etc.
