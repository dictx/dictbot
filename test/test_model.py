import unittest

from bot import store

def client():
    from mockfirestore import client
    return client.MockFirestore()

store.ds = client()

class ModelTest(unittest.TestCase):
    def setUp(self):
        store.ds.reset()
        
    def testModel(self):
        class TestModel(store.Model):
            props = 'x', 'y', 'z', 'w'
        vals = dict(x=1, y=2, w=4)
        test = TestModel(**vals)

        self.assertEqual(test.x, 1)
        self.assertEqual(test.z, None)
        self.assertEqual(test.w, 4)

        doc = test.dump()
        self.assertEqual('x' in doc, True)
        self.assertEqual('z' in doc, False)

    def testServer(self):
        vals = dict(dbases=3, strats=4)
        server = store.Server(**vals)

        self.assertEqual(server.id, None)
        self.assertEqual(server.ident, None)
        self.assertEqual(server.dbases, 3)

        doc = server.dump()
        self.assertEqual('dbases' in doc, True)
        self.assertEqual('ident' in doc, False)

    def testStore(self):
        vals = dict(ident='Test Server')
        server = store.Server(**vals)
        server.dbases = 7
        server.strats = 4
        server.set('test')

        new = store.Server.get('test')
        self.assertEqual(new.host, 'test')
        self.assertNotEqual(new.ident, None)
        self.assertEqual(new.capbs, None)
        self.assertEqual(new.dbases, 7)

        new.capbs = 'auth.mime'
        new.put()

        old = store.Server.get('test')
        self.assertEqual(old.host, 'test')
        self.assertNotEqual(old.ident, None)
        self.assertNotEqual(old.capbs, None)

        new.delete()
        old = store.Server.get('test')
        self.assertEqual(old, None)

    def testState(self):
        for x in range(20):
            ref = 'host-%s' % x if x else 'test'
            vals = dict(current='up')
            state = store.State(**vals)
            state.set(ref)
        docs = store.State.all()
        self.assertEqual(len(docs), 20)

        server = store.Server(ident='')
        server.set('test')

        state = store.State.get('test')
        self.assertNotEqual(state, None)
        self.assertEqual(state.host, 'test')

        server = state.server
        self.assertEqual(server.id, 'test')
        self.assertEqual(server.host, 'test')

        state = server.state
        self.assertEqual(state.host, 'test')

    def testLog(self):
        for _ in range(20):
            log = store.Log()
            log.put()

        doc = store.Log.current()
        self.assertNotEqual(doc, None)
        date = str(doc.date).split()[0]
        self.assertEqual(len(date), 10)

    def testHist(self):
        vals = dict(ident='Test Server')
        server = store.Server(**vals)
        server.set('test')

        for x in range(101):
            state = 'up' if x % 8 else 'down'
            if x == 100: state = 'unknown'
            vals = dict(host='test', state=state)
            hist = store.Hist(**vals)
            hist.put()

        hist = store.Hist.server('test')
        docs = list(hist.get())
        self.assertEqual(len(docs), 101)

        doc = store.Hist.first(hist)
        self.assertNotEqual(doc, None)
        date = str(doc.date).split()[0]
        self.assertEqual(len(date), 10)

        count = store.Count('test', 1)
        self.assertEqual(count.state('up'), 87)
        self.assertEqual(count.state('down'), 13)
        self.assertEqual(count.state('unknown'), 1)

        state = store.State.new('test')
        state.update(1)
        self.assertEqual(state.avail, 86)
        self.assertEqual(state.ups, 87)
        self.assertEqual(state.downs, 14)

    def testConf(self):
        vals = dict(days=360, hours=23, mins=5, secs=123)
        conf = store.Conf(**vals)
        conf.set('test')

        doc = store.Conf.get('test')
        self.assertEqual(doc.hours, 23)
        with self.assertRaises(AttributeError):
            self.assertEqual(doc.secs, 23)
