import unittest
from unittest import mock

from bot import tracker, tasks, remote, dictbot, store, site

site.setup()

def client():
    from mockfirestore import client
    return client.MockFirestore()

store.ds = client()

class TrackerTest(unittest.TestCase):
    def setUp(self):
        store.ds.reset()
        tasks.project = 'test'
        self.ct = tasks.ct = mock.Mock()

    def testInfo(self):
        info = tracker.Info('localhost:2629')
        self.assertEqual(info.addr(), '127.0.0.1')

    def testState(self):
        server = store.Server(ident='Test')
        server.set('localhost')

        for _ in range(20):
            vals = dict(host='localhost', state='up')
            hist = store.Hist(**vals)
            hist.put()

        test = dictbot.State()
        test.process('1')

        state = store.State.get('localhost')
        self.assertEqual(state.ups, 20)
        self.assertEqual(state.downs, 0)
        self.assertEqual(state.avail, 100)

    def testTracker(self):
        tracker = remote.Tracker('localhost')
        result = tracker.run()

        self.assertEqual(len(result), 3)
        server, dbases, strats = result
        self.assertEqual(len(server), 6)
        self.assertEqual(server[4], '16')
        self.assertEqual(server[5], '13')
        self.assertEqual(len(dbases), 16)
        self.assertEqual(len(strats), 13)

        data = '\n'.join('|'.join(val) for val in result)
        test = dictbot.Store()
        test.process(data)

        server = store.Server.get('localhost')
        self.assertEqual(server.host, 'localhost')
        self.assertEqual(server.dbases, 16)
        self.assertEqual(server.strats, 13)

        hist = server.current()
        self.assertEqual(hist.state, 'up')

    def testDown(self):
        tracker = remote.Tracker('localhost:2629')
        result = tracker.run()

        self.assertEqual(len(result), 1)
        server = result[0]
        self.assertEqual(len(server), 6)
        self.assertEqual(server[4], '')
        self.assertEqual(server[5], '')

        data = '\n'.join('|'.join(val) for val in result)
        test = dictbot.Store()
        test.process(data)

        server = store.Server.get('localhost:2629')
        self.assertEqual(server.host, 'localhost:2629')
        self.assertEqual(server.dbases, 0)
        self.assertEqual(server.strats, 0)

        server.dbases = 1
        server.strats = 2
        server.put()

        test.process(data)
        old = store.Server.get('localhost:2629')
        self.assertEqual(old.dbases, 1)
        self.assertEqual(old.strats, 2)

        hist = server.current()
        self.assertEqual(hist.state, 'down')

    def testRemote(self):
        conn = remote.Connection('test')
        ident = 'Test Server <test> <ident>'
        conn.identification(ident)

        info = conn.info
        self.assertEqual(info.ident, 'Test Server')
        self.assertEqual(info.capbs, 'test')

        conn = remote.Connection('test')
        conn.identification(ident[:13])

        info = conn.info
        self.assertEqual(info.ident, 'Test Server')
        self.assertEqual(info.capbs, '')

        conn = remote.Connection('test')
        conn.identification('')

        info = conn.info
        self.assertEqual(info.ident, '')
        self.assertEqual(info.capbs, '')

    def testRead(self):
        conn = remote.Connection('test')

        conn.state = remote.State.Reading
        conn.response = ''
        text = 'test'
        conn.read(text)
        self.assertEqual(conn.state, remote.State.Reading)
        self.assertEqual(conn.response, '')

        text = '110 1 database found\ntest "Test Dict"\n.\n'
        conn.read(text)
        self.assertEqual(conn.state, remote.State.Reading)
        self.assertNotEqual(conn.response, '')
        self.assertEqual(len(conn.response.list), 1)
        self.assertEqual(conn.response.list[0], 'Test Dict')

        text = '123 test'
        conn.read(text)
        self.assertEqual(conn.state, remote.State.Error)

    def testParse(self):
        parser = remote.Parser()

        count = parser.count('10 databases found')
        self.assertEqual(count, 10)

        count = parser.count('no databases found')
        self.assertEqual(count, 0)

        code = parser.code('250 ok')
        self.assertEqual(code, 250)

        code = parser.count('bad msg')
        self.assertEqual(code, 0)

        message = parser.message('250 ok')
        self.assertEqual(message, 'ok')

        message = parser.message('bad')
        self.assertEqual(message, '')

        end = parser.end('.')
        self.assertEqual(end, True)

        end = parser.end('good')
        self.assertEqual(end, False)
