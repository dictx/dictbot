import unittest
from unittest import mock

from bot import tracker, tasks, store, site

site.setup()

def client():
    from mockfirestore import client
    return client.MockFirestore()

store.ds = client()

from conf import fill

class TasksTest(unittest.TestCase):
    def setUp(self):
        store.ds.reset()
        tasks.project = 'test'
        self.ct = tasks.ct = mock.Mock()

    def testTask(self):
        task = tasks.Task('/state')
        task.schedule('1')

        self.assertEqual(self.ct.queue_path.called, True)
        self.assertEqual(self.ct.queue_path.call_count, 1)

        args = self.ct.queue_path.call_args[0]
        self.assertEqual(len(args), 3)
        self.assertEqual(args[0], 'test')

        self.assertEqual(self.ct.create_task.called, True)
        self.assertEqual(self.ct.create_task.call_count, 1)

    def testSchedule(self):
        conf = store.Conf(mins=5)
        conf.set('site')

        task = tracker.Schedule()
        task.view('1')

        self.assertEqual(self.ct.create_task.called, True)
        self.assertEqual(self.ct.create_task.call_count, 1)

    def testUpdate(self):
        conf = store.Conf(days=5)
        conf.set('site')

        fill.host('localhost')
        fill.host('dict.org')
        update = tracker.Update()
        update.process('1')

        self.assertEqual(self.ct.create_task.called, True)
        self.assertEqual(self.ct.create_task.call_count, 3)

    def testHost(self):
        host = tracker.Host()
        host.view('localhost')

        self.assertEqual(self.ct.create_task.called, True)
        self.assertEqual(self.ct.create_task.call_count, 1)

    def testTime(self):
        t1 = tasks.timestamp(0)
        t2 = tasks.timestamp(100)

        self.assertEqual(t2.seconds > t1.seconds, True)
        self.assertEqual(t2.seconds - t1.seconds, 100)
