from bot import store

def site():
    for x in range(20):
        ref = '%s.hostname.domain' % x
        state = 'up' if x % 7 else 'down'
        if x == 10: state = 'unknown'
        vals = dict(current=state, ups=2 * x + 3, downs=x // 4)
        state = store.State(**vals)
        state.set(ref)

    for x in range(20):
        ref = '%s.hostname.domain' % x
        state = 'up' if x % 7 else 'down'
        if x == 10: state = 'unknown'
        vals = dict(state=state, host=ref)
        hist = store.Hist(**vals)
        hist.put()

    for x in range(20):
        ref = '%s.hostname.domain' % x
        if x % 5: capbs = 'auth.mime'
        else: capbs = 'mime.markup.xversion.xlev.markup-none.markup-wiki'
        vals = dict(ident='Test Server', capbs=capbs, dbases=2 * x + 1, strats=3 * x + 2)
        host = store.Server(**vals)
        host.set(ref)

    for x in range(20):
        ref = '%s.hostname.domain' % x
        dblist = ['Test Dict', 'Best Dict', 'Pest Dict']
        strats = ['all', 'any', 'none']
        if x % 5: dblist = strats = []
        vals = dict(dblist=dblist, stratlist=strats)
        info = store.Info(**vals)
        info.set(ref)

def log():
    log = store.Log.current()
    if not log:
        store.Log().put()

def conf():
    vals = dict(days=360, hours=23, mins=4, weeks=7)
    conf = store.Conf(**vals)
    conf.set('site')
    
def block():
    bots = ['good', 'bad', 'ugly']
    nets = ['1.2.3.4/24', '172.0.0.1/16']
    vals = dict(bots=bots, nets=nets)
    block = store.Block(**vals)
    block.set('site')

def auth(val):
    vals = dict(val=val)
    auth = store.Auth(**vals)
    auth.set('site')

def host(name):
    server = store.Server(ident='Test Server')
    server.set(name)

def patch():
    try:
        store.ds.get_all
    except AttributeError:
        store.ds.get_all = lambda refs: [ref.get() for ref in refs]
