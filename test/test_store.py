import unittest

from bot import dictbot, store

def client():
    from mockfirestore import client
    return client.MockFirestore()

store.ds = client()

data = """test|up|Test Server|auth.mime|7|4
Test Dict|Best Dict|Pest Dict
all|any|none|many|some"""

class StoreTest(unittest.TestCase):
    def setUp(self):
        store.ds.reset()

    def testStore(self):
        test = dictbot.Store()
        self.assertNotEqual(test, None)

    def testData(self):
        test = dictbot.Store()
        test.process(data)
        
        server = store.Server.get('test')
        self.assertEqual(server.host, 'test')
        self.assertEqual(server.ident, 'Test Server')
        self.assertEqual(server.capbs, 'auth.mime')
        self.assertEqual(server.dbases, 7)
        self.assertEqual(server.strats, 4)

        info = store.Info.get('test')
        self.assertNotEqual(info, None)
        self.assertEqual(len(info.dblist), 3)
        self.assertEqual(info.dblist[0], 'Test Dict')
        self.assertEqual(len(info.stratlist), 5)
        self.assertEqual(info.stratlist[0], 'all')
        
    def testHost(self):
        test = dictbot.Store()
        test.process(data.splitlines()[0])

        server = store.Server.get('test')
        self.assertEqual(server.host, 'test')

        info = store.Info.get('test')
        self.assertEqual(info, None)

        hist = store.Hist.server('test').get()
        result = list(hist)
        self.assertEqual(len(result), 1)
        doc = result[0]
        self.assertEqual(doc.exists, True)
        self.assertNotEqual(doc.reference, None)

    def testState(self):
        test = dictbot.Store()
        test.process(data)

        test = dictbot.State()
        test.process(1)

        state = store.State.get('test')
        self.assertEqual(state.host, 'test')
        self.assertEqual(state.current, 'up')
        self.assertEqual(state.ups, 1)
        self.assertEqual(state.downs, 0)
        self.assertEqual(state.avail, 100)

        server = state.server
        self.assertNotEqual(server, None)

        hist = server.current()
        self.assertEqual(hist.state, 'up')
